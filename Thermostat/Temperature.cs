﻿using System;

namespace Thermostat
{
    public class Temperature : ICloneable
    {
        private double temperatureInCelsius;

        public Temperature(double temperatureInCelsius)
        {
            AppSettings = AppSettings.GetSettings();
            TemperatureInCelsius = temperatureInCelsius;
        }

        public double TemperatureInCelsius
        {
            get => temperatureInCelsius;
            private set 
                => temperatureInCelsius = value <= AppSettings.MaximalTemperatureInCelsius && value >= AppSettings.MinimalTemperatureInCelsius 
                    ? value 
                    : throw new ArgumentOutOfRangeException(nameof(value), 
                        $"Temperature must be in range from {AppSettings.MinimalTemperatureInCelsius} to {AppSettings.MaximalTemperatureInCelsius}.");
        }
        
        private AppSettings AppSettings { get; }

        public bool Equals(Temperature temperature)
            =>  temperature is { } 
                && Math.Abs(Math.Round(TemperatureInCelsius, AppSettings.RoundCountOfDigits, MidpointRounding.AwayFromZero)
                            - Math.Round(temperature.TemperatureInCelsius, AppSettings.RoundCountOfDigits, MidpointRounding.AwayFromZero)) 
                < Math.Round(AppSettings.TemperatureAccuracy, AppSettings.RoundCountOfDigits,  MidpointRounding.AwayFromZero);

        public override bool Equals(object temperatureObject)
            => Equals(temperatureObject as Temperature);

        public override int GetHashCode()
        {
            int hash = 17;
            
            return hash * Math.Round(TemperatureInCelsius, 1, MidpointRounding.AwayFromZero).GetHashCode();
        }

        public override string ToString()
            => $"{Math.Round(TemperatureInCelsius, AppSettings.RoundCountOfDigits, MidpointRounding.AwayFromZero)} \u2103";

        public object Clone() => new Temperature(TemperatureInCelsius);

        public static bool operator ==(Temperature lhs, Temperature rhs)
            => lhs is null && rhs is null || lhs is { } && lhs.Equals(rhs);

        public static bool operator !=(Temperature lhs, Temperature rhs) => !(lhs == rhs);

        public static bool operator <(Temperature lhs, Temperature rhs) => !IsMore(lhs, rhs) && lhs != rhs;

        public static bool operator >(Temperature lhs, Temperature rhs) => IsMore(lhs, rhs);

        public static double operator -(Temperature lhs, Temperature rhs)
            => Subtract(lhs, rhs);
        
        public static double operator +(Temperature lhs, Temperature rhs)
            => Add(lhs, rhs);

        public static bool IsMore(Temperature lhs, Temperature rhs)
        {
            bool isMore;
            
            if (lhs is null)
            {
                isMore = false;
            }
            else if (rhs is null)
            {
                isMore = true;
            }
            else
            {
                isMore = Math.Round(lhs.TemperatureInCelsius, lhs.AppSettings.RoundCountOfDigits, MidpointRounding.AwayFromZero) 
                    - Math.Round(rhs.TemperatureInCelsius, rhs.AppSettings.RoundCountOfDigits, MidpointRounding.AwayFromZero) >= 
                    Math.Round(lhs.AppSettings.TemperatureAccuracy, lhs.AppSettings.RoundCountOfDigits,  MidpointRounding.AwayFromZero);
                // TODO dependencies
            }

            return isMore;
        }

        public static double Subtract(Temperature lhs, Temperature rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.TemperatureInCelsius - rhs.TemperatureInCelsius;
        }
        
        public static double Add(Temperature lhs, Temperature rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.TemperatureInCelsius - rhs.TemperatureInCelsius;
        }
    }
}