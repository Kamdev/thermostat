﻿using System;

namespace Thermostat
{
    public class AppSettings
    {
        private static AppSettings appSettings;

        public static AppSettings GetSettings()
            => appSettings ??= new AppSettings();

        private AppSettings()
        {
            MaximalTemperatureInCelsius = 100;
            MinimalTemperatureInCelsius = -100;
            TemperatureAccuracy = 0.1;
            RoundCountOfDigits = 1;
        }

        public void ChangeTemperatureAccuracy(double temperatureAccuracy)
        {
            if (temperatureAccuracy < 0)
            {
                throw new ArgumentException("Accuracy must be more or equals zero.");
            }
            
            TemperatureAccuracy = temperatureAccuracy;
        }
        
        public void ChangeRoundCountOfDigits(int roundCountOfDigits)
        {
            if (roundCountOfDigits < 0)
            {
                throw new ArgumentException("Count of digits must be more or equals zero.");
            }
            
            RoundCountOfDigits = roundCountOfDigits;
        }
        
        public double MaximalTemperatureInCelsius { get; private set; }
        
        public double MinimalTemperatureInCelsius { get; private set; }

        public double TemperatureAccuracy { get; private set; }
        
        public int RoundCountOfDigits { get; private set; }
    }
}