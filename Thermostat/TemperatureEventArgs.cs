﻿using System;

namespace Thermostat
{
    public class TemperatureEventArgs : EventArgs
    {
        private Temperature temperature;
        
        public TemperatureEventArgs(Temperature temperature) 
            => Temperature = temperature;

        public Temperature Temperature 
        { 
            get => temperature;
            private init => temperature = value ?? throw new ArgumentNullException(nameof(value), "Temperature is null.");
        }
    }
}