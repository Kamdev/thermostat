﻿using System;

namespace Thermostat.TemperatureTrackers
{
    public class Thermostat : ITemperatureTracker
    {
        private readonly Random random = new Random(Environment.TickCount);

        public Thermostat() => EmulateTemperatureChange();
        
        private Temperature currentTemperature;

        public Thermostat(Temperature currentTemperature)
            => CurrentTemperature = currentTemperature;

        public Temperature CurrentTemperature 
        { 
            get => currentTemperature;
            private set => currentTemperature = value ?? throw new ArgumentNullException(nameof(value), "Temperature is null.");
        }
        
        public event EventHandler<TemperatureEventArgs> TemperatureChanged;
        
        public void UpdateTemperature(Temperature temperature)
        {
            if (temperature != CurrentTemperature)
            {
                CurrentTemperature = temperature;
            }
            
            var temperatureEventArgs = new TemperatureEventArgs(temperature);
            
            TemperatureChanged?.Invoke(this, temperatureEventArgs);
        }
        
        public void EmulateTemperatureChange() => CurrentTemperature = new Temperature(random.Next(-20,50));
    }
}