﻿using System;

namespace Thermostat.TemperatureTrackers
{
    public interface ITemperatureTracker
    {
        event EventHandler<TemperatureEventArgs> TemperatureChanged;

        void UpdateTemperature(Temperature temperature);
    }
}