﻿using System;
using Thermostat.TemperatureTrackers;

namespace Thermostat.TemperatureManagers
{
    public abstract class TemperatureController
    {
        private Temperature switchOnTemperature;
        
        public Temperature SwitchOnTemperature
        {
            get => switchOnTemperature;
            private set
                => switchOnTemperature = value ?? throw new ArgumentNullException(nameof(value));
        }

        protected TemperatureController(Temperature switchOnTemperature)
        {
            SwitchOnTemperature = switchOnTemperature;
        }

        public abstract void ManageTemperature(object sender, TemperatureEventArgs temperatureEventArgs);
        
        public abstract void On(ITemperatureTracker tracker);
    }
}