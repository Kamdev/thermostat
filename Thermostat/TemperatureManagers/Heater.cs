﻿using Thermostat.TemperatureTrackers;

namespace Thermostat.TemperatureManagers
{
    public class Heater : TemperatureController
    {
        public Heater(Temperature switchOnTemperature) : base(switchOnTemperature)
        {
        }

        public override void ManageTemperature(object sender, TemperatureEventArgs temperatureEventArgs)
        {
            if (temperatureEventArgs.Temperature < SwitchOnTemperature)
            {
                On(sender as ITemperatureTracker);
            }
        }

        public override void On(ITemperatureTracker tracker) 
            => tracker?.UpdateTemperature(SwitchOnTemperature);
    }
}