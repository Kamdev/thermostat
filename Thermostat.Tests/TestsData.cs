﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Thermostat.Tests
{
    public sealed class TestsData
    {
        public static IEnumerable<TestCaseData> TestCasesForEquals
        {
            get
            {
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
                yield return new TestCaseData(new Temperature(17.2), new Temperature(18.5)).Returns(false);
                yield return new TestCaseData(new Temperature(-15.6), new Temperature(-16.6)).Returns(false);
                yield return new TestCaseData(new Temperature(5.76), new Temperature(5.86)).Returns(true);
                yield return new TestCaseData(new Temperature(5.76), new Temperature(5.36)).Returns(false);
                yield return new TestCaseData(new Temperature(75.13), new Temperature(75.1432)).Returns(true);
                yield return new TestCaseData(new Temperature(0.1312), new Temperature(0)).Returns(true);
                yield return new TestCaseData(new Temperature(-0.8), null).Returns(false);
            }
        }
        
        public static IEnumerable<TestCaseData> TestCasesForEqualsWithObject
        {
            get
            {
                yield return new TestCaseData(new Temperature(10.2), "ten").Returns(false);
                yield return new TestCaseData(new Temperature(24), 75.4).Returns(false);
                yield return new TestCaseData(new Temperature(-24), new Temperature(24)).Returns(false);
                yield return new TestCaseData(new Temperature(17.2), new Temperature(18.5)).Returns(false);
                yield return new TestCaseData(new Temperature(15.6), new Temperature(15.6)).Returns(true);
                yield return new TestCaseData(new Temperature(2.1), new Temperature(2.13)).Returns(true);
                yield return new TestCaseData(new Temperature(15.1), new Temperature(15.17)).Returns(true);
                yield return new TestCaseData(new Temperature(-15.27), new Temperature(-15.16)).Returns(true);
                yield return new TestCaseData(new Temperature(-15.27), new Temperature(-15.17)).Returns(true);
                yield return new TestCaseData(new Temperature(-15.27), null).Returns(false);
            }
        }
        
        public static IEnumerable<TestCaseData> TestCasesForMore
        {
            get
            {
                yield return new TestCaseData(null, null).Returns(false);
                yield return new TestCaseData(null, new Temperature(73.709)).Returns(false);
                yield return new TestCaseData(new Temperature(8), null).Returns(true);
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
                yield return new TestCaseData(new Temperature(12.8), new Temperature(12.1)).Returns(true);
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
                yield return new TestCaseData(new Temperature(-6), new Temperature(6)).Returns(false);
            }
        }
    }
}