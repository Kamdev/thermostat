using NUnit.Framework;

namespace Thermostat.Tests
{
    [SetUpFixture]
    public class TestsSettings
    {
        private readonly AppSettings settings = AppSettings.GetSettings();
        
        [OneTimeSetUp]
        public void Settings()
        {
            settings.ChangeTemperatureAccuracy(1);
            settings.ChangeRoundCountOfDigits(0);
        }
    }
    
    [TestFixture]
    public class TemperatureTests
    {
        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForEqualsWithObject))]
        public bool Equals_WithObjectParameter_ReturnsStatement(Temperature temperature, object obj)
            => temperature.Equals(obj);
        
        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForEquals))]
        public bool Equals_ReturnsStatement(Temperature temperature, Temperature other)
            => temperature.Equals(other);
        
        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForEquals))]
        public bool OperatorEqualsTests(Temperature lhs, Temperature rhs)
            => lhs == rhs;

        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForEquals))]
        public bool OperatorNotEqualsTests(Temperature lhs, Temperature rhs)
            => !(lhs != rhs);
        
        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForMore))]
        public bool OperatorIsMoreTests(Temperature lhs, Temperature rhs)
            => lhs > rhs;

        [TestCaseSource(typeof(TestsData), nameof(TestsData.TestCasesForMore))]
        public bool OperatorIsLessTests(Temperature lhs, Temperature rhs)
            => lhs > rhs;
    }
}